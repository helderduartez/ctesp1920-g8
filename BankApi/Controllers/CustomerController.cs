﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using BankApi.Infrastructure;
using BankApi.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BankApi.Controllers
{
    [Produces("application/json")]
    [Route("BankAccount/")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private readonly Infrastructure.DbApiContext _context;
        public CustomerController(Infrastructure.DbApiContext context)
        {
            _context = context;
        }
        
        [HttpGet("Get/{id}")]
        public IActionResult GetId(int? id)
        {
            if (id is null)
            {
                return BadRequest("Id nulo!");
            }
            
            
            var response = _context.Account.Where(x => x.AccountId == id)
                   .Select(
                   (c => new
                   {
                       c.Customer.Name,
                       c.Customer.Address,
                       c.Customer.Contact,
                       c.Balance,                        
                    }));

            if (response.Count() == 0)
            {
                return NotFound();
            }

            return Ok(response);
        }

    }
}
