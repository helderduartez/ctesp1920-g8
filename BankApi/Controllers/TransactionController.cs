﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using BankApi.Infrastructure;
using BankApi.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BankApi.Controllers
{
    [Produces("application/json")]
    [Route("BankAccount/")]
    [ApiController]
    public class TransactionController : ControllerBase
    {

        private readonly Infrastructure.DbApiContext _context;
        public TransactionController(Infrastructure.DbApiContext context)
        {
            _context = context;
        }

        [HttpPost("Post")]
        public async Task<ActionResult<Transaction>> Add(TransactionDTO transaction)
        {
            if (transaction==null)
            {
                return BadRequest("Fala id da conta, tipo de transação, valor ou data!");
            }
            try
            {
                Transaction transactionToAdd = new Transaction
                {
                    TransactionId = await _context.Transaction.MaxAsync(x => x.TransactionId) + 1,
                    Account = await _context.Account.SingleOrDefaultAsync(x => x.AccountNumber == transaction.AccountId),
                    TransactionType = await _context.TransactionType.SingleOrDefaultAsync(x => x.TypeId == transaction.TypeId),
                    Date = transaction.Date,
                    Amount = transaction.Amount
                };


                if(transactionToAdd.TransactionType.TypeId==1 && transactionToAdd.Amount > transactionToAdd.Account.Balance)
                {
                    return BadRequest("Débito superior a saldo!");
                }
                else if(transactionToAdd.TransactionType.TypeId == 1)
                {
                    transactionToAdd.Account.Balance -= transaction.Amount;
                }
                else if (transactionToAdd.TransactionType.TypeId == 2)
                {
                    transactionToAdd.Account.Balance += transaction.Amount;
                }

                _context.Transaction.Add(transactionToAdd);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Get/{id}/Transação")]
        public IActionResult GetId(int? id)
        {
            if (id == null)
            {
                return BadRequest("Id nulo!");
            }
            try
            {
                var account = _context.Account.Where(x => x.AccountId == id);
                var list = _context.Transaction.Where(x => x.Account.AccountId == id)
                    .Select(c => new TransactionDTO
                    {
                        Date = c.Date,
                        Amount = c.Amount,
                        TypeId = _context.TransactionType.First(x => x.TypeId == c.TransactionType.TypeId).TypeId
                    });
                var dates = new SortedDictionary<string, double> { };
                DateTime thirtyDaysAgo = DateTime.Today.AddDays(-30);
                foreach (var trans in list)
                {
                    if (thirtyDaysAgo < trans.Date)
                    {
                        var amount = trans.Amount;
                        if (trans.TypeId == 1)
                        {
                            amount = 0 - amount;
                        }
                        if (!dates.ContainsKey(trans.Date.ToString("yyyy-MM-dd")))
                        {

                            dates.Add(trans.Date.ToString("yyyy-MM-dd"), amount);
                        }
                        else if (dates.ContainsKey(trans.Date.ToString("yyyy-MM-dd")))
                        {
                            dates[trans.Date.ToString("yyyy-MM-dd")] += amount;
                        }
                    }

                }

                var dates2 = new SortedDictionary<string, double> { };
                double balance = account.First().Balance;
                foreach (var item in dates.OrderBy(i => i.Key))
                {
                    dates2.Add(item.Key, balance + item.Value);
                    balance += item.Value;
                }


                var EndDayOfBalances = new
                {
                    EndDayOfBalances = dates2.Select(item => new
                    {
                        Date = DateTime.ParseExact(item.Key, "yyyy-MM-dd", CultureInfo.CurrentCulture).ToString("yyy-MM-dd"),
                        Balance = item.Value
                    })

                    .Distinct()
                    .OrderByDescending(d => d.Date)
                };

                var response = _context.Account.Where(x => x.AccountId == id)
                   .Select(
                   (c => new
                   {
                       TotalCredits = c.Transactions.Count(x => x.TransactionType.TypeId == 2),
                       TotalDebits = c.Transactions.Count(x => x.TransactionType.TypeId == 1),
                       EndDayOfBalances
                   }));
                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
