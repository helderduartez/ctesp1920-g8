﻿using BankApi.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BankApi.Infrastructure
{
    public class DbApiContext : DbContext
    {
        public DbApiContext(DbContextOptions<DbApiContext> options) : base(options)
        {
            
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<TransactionType> TransactionType { get; set; }
        private static void AddTestData(DbApiContext context)
        {
            List<Customer> customers = new List<Customer>();
            List<Account> accounts = new List<Account>();
            List<Transaction> transactions = new List<Transaction>();
            List<TransactionType> transactionTypes = new List<TransactionType>();
            transactionTypes.Add(new TransactionType
            {
                TypeId = 1,
                Description = "Débito"
            });
            transactionTypes.Add(new TransactionType
            {
                TypeId = 2,
                Description = "Crédito"
            });
            customers.Add(new Customer
            {
                CustomerId = 1,
                Name = "Cliente",
                Address = "Rua do Cliente",
                Contact = "9169696969"
            });
            accounts.Add(new Account
            {
                AccountId = 1,
                Customer = customers.Find(x => x.CustomerId == 1),
                AccountNumber = 1234567890,
                IBAN = "217093862s89d079873012",
                Balance = 70

            });
            transactions.Add(new Transaction
            {
                TransactionId = 1,
                Account = accounts.Find(x => x.AccountId == 1),
                Date = DateTime.UtcNow.AddDays(-5),
                Amount = 3.99,
                TransactionType = transactionTypes.Find(x => x.TypeId == 2),
            });
            transactions.Add(new Transaction
            {
                TransactionId = 2,
                Account = accounts.Find(x => x.AccountId == 1),
                Date = DateTime.UtcNow.AddDays(-4),
                Amount = 4.99,
                TransactionType = transactionTypes.Find(x => x.TypeId == 1),
            });
            transactions.Add(new Transaction
            {
                TransactionId = 3,
                Account = accounts.Find(x => x.AccountId == 1),
                Date = DateTime.UtcNow.AddDays(-4),
                Amount = 4.99,
                TransactionType = transactionTypes.Find(x => x.TypeId == 1),
            });
            transactions.Add(new Transaction
            {
                TransactionId = 4,
                Account = accounts.Find(x => x.AccountId == 1),
                Date = DateTime.UtcNow.AddDays(-30),
                Amount = 4.99,
                TransactionType = transactionTypes.Find(x => x.TypeId == 1),
            });
            transactions.Add(new Transaction
            {
                TransactionId = 5,
                Account = accounts.Find(x => x.AccountId == 1),
                Date = DateTime.UtcNow.AddDays(-31),
                Amount = 4.99,
                TransactionType = transactionTypes.Find(x => x.TypeId == 1),
            });

            context.Customers.AddRange(customers);
            context.Account.AddRange(accounts);
            context.Transaction.AddRange(transactions);
            context.TransactionType.AddRange(transactionTypes);
            context.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            List<Customer> customers = new List<Customer>();
            modelBuilder.Entity<TransactionType>().HasData(
                new TransactionType
                {
                    TypeId = 1,
                    Description = "Débito"
                },
                new TransactionType
                {
                    TypeId = 2,
                    Description = "Crédito"
                }
            );
            modelBuilder.Entity<Customer>().HasData(
                new Customer
                {
                    CustomerId = 1,
                    Name = "Cliente",
                    Address = "Rua do Cliente",
                    Contact = "919191919"
                }
            );
        }
    }

}
