﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BankApi.Model
{
    public class Account
    {
        [Key]
        public int AccountId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        public int AccountNumber { get; set; }
        public string IBAN { get; set; }
        public double Balance { get; set; }
        public virtual List<Transaction> Transactions { get; set; }
    }
}
