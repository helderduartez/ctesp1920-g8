﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BankApi.Model
{
    public class Customer
    {   [Key]
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
    }
}
