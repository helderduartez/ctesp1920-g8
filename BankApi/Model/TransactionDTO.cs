﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BankApi.Model
{
    public class TransactionDTO
    {
        public int AccountId { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public int TypeId { get; set; }
    }
}
