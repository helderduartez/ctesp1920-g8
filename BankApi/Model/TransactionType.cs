﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BankApi.Model
{
    public class TransactionType
    {
        [Key]
        public int TypeId { get; set; }
        public string Description { get; set; }
    }
}
