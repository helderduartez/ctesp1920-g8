﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BankApi.Model
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }        
        [ForeignKey("TypeId")]
        public virtual TransactionType TransactionType { get; set; }
    }
}
