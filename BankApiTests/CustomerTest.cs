using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankApi.Infrastructure;
namespace BankApiTests
{
    [TestClass]
    public class CustomerTest
    {

        private TestMethods testMethod = new TestMethods();

        //Teste se o id for null
        [TestMethod]
        public void TestGetIdNull()
        {            
            Assert.AreEqual(-1, testMethod.GetId(null));
        }

        //Teste se o id for diferente de null
        [TestMethod]
        public void TestGetId()
        {
            Assert.AreEqual(1, testMethod.GetId(1));
        }
    }
}
