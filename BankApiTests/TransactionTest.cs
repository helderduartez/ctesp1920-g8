using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankApi.Infrastructure;
using BankApi.Model;
using System;

namespace BankApiTests
{
    [TestClass]
    public class TransactionTest
    {
        private TestMethods testMethod = new TestMethods();

        //Teste get se o id for null
        [TestMethod]
        public void TestGetIdNull()
        {            
            Assert.AreEqual(-1, testMethod.GetArray(null));
        }

        //Teste get se a resposta for criada
        [TestMethod]
        public void TestGetId()
        {
            Assert.AreEqual(1, testMethod.GetArray(1));
        }

        //Teste post se a transacao for null
        [TestMethod]
        public void TestPostTransactionNull()
        {

            Assert.AreEqual(-1, testMethod.Add(null,1));
        }

        //Teste post se o valor de transacao for maior que o saldo
        [TestMethod]
        public void TestPostBalance()
        {
            double balance = 0;
            TransactionDTO transactionDTO = new TransactionDTO
            {
                AccountId = 1,
                Amount = 1,
                Date = DateTime.UtcNow.AddDays(-4),
                TypeId = 1
            };

            Assert.AreEqual(-2, testMethod.Add(transactionDTO, balance));
        }

        //Teste post se a transacao for um debito
        [TestMethod]
        public void TestPostDebit()
        {
            double balance = 1;
            TransactionDTO transactionDTO = new TransactionDTO
            {
                AccountId = 1,
                Amount = 1,
                Date = DateTime.UtcNow.AddDays(-4),
                TypeId = 1
            };

            Assert.AreEqual(0, testMethod.Add(transactionDTO, balance));
        }

        //Teste post se a transacao for um credito
        [TestMethod]
        public void TestPostCredit()
        {
            double balance = 0;
            TransactionDTO transactionDTO = new TransactionDTO
            {
                AccountId = 1,
                Amount = 1,
                Date = DateTime.UtcNow.AddDays(-4),
                TypeId = 2
            };

            Assert.AreEqual(1, testMethod.Add(transactionDTO, balance));
        }
    }
}
