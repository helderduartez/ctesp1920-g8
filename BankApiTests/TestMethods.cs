﻿using BankApi.Infrastructure;
using BankApi.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BankApiTests
{
    class TestMethods
    {
        public TestMethods()
        {
        }
        public int GetId(int? id)
        {
            if (id is null)
            {
                return -1;
            }

            return 1;
        }

        public double Add(TransactionDTO transaction, double balance)
        {
            if (transaction==null)
            {
                return -1;
            }
            try
            {

                if (transaction.TypeId == 1 && transaction.Amount > balance)
                {
                    return -2;
                }
                else if (transaction.TypeId == 1)
                {
                    balance -= transaction.Amount;
                }
                else if (transaction.TypeId == 2)
                {
                    balance += transaction.Amount;
                }

                return balance;
            }
            catch (Exception)
            {
                return -3;
            }
        }

        public int GetArray(int? id)
        {
            if (id == null)
            {
                return -1;
            }
            
            return 1;
                       
        }
    

    }
}
